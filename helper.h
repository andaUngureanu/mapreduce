#ifndef HELPER
#define HELPER

#define NAME_SIZE 20
#define PATH_SIZE 30
#define MASTER 0

typedef struct FILECOUNT
{
char *fileName;
int no;	
}FileCount;

typedef struct FILEMAP
{
char *word;
int fileCountNo;
FileCount* fileCounts;	
}FileMap;

typedef struct MAPRED
{
FileMap* fileMaps;
int size;	
}MapRed;

typedef struct TERMCOUNT
{
char *word;
int no;	
}TermCount;

typedef struct WordMap
{
int size;
TermCount *termCounts;
}WordMap;

void printWordMap(WordMap *map,int rank)
{
	printf("%d -map %d {",rank,map->size);
	for(int i=0;i<map->size;++i)
	{
		printf("%s-%d; ",map->termCounts[i].word,map->termCounts[i].no);
	}
	printf("}\n");
}

int match(char *inText, char *pattern)
{
   int position = 0;
   char *in, *pat;
 
   in = inText;
   pat = pattern;
 
   while(*inText)
   {
      while(*in==*pat)
      {
         in++;
         pat++;
         if(*in=='\0'||*pat=='\0')
            break;         
      }   
      if(*pat=='\0')
         break;
 
      inText++;
      position++;
      in = inText;
      pat = pattern;
   }
   if(*inText)
      return position;
   else   
      return -1;   
}


void printMapRed(MapRed* map, char* filePath)
{
	FILE *fout=NULL;
	fout=fopen(filePath,"w+");
	if(fout == NULL)
	{
		printf("eroare scriere %s\n",filePath);
		return;
	}
	for(int i=0;i<map->size;++i)
	{
		fprintf(fout,"%s [",map->fileMaps[i].word);
		for(int j=0;j<map->fileMaps[i].fileCountNo;++j)
		{
			fprintf(fout,"{%s-%d} ",map->fileMaps[i].fileCounts[j].fileName,map->fileMaps[i].fileCounts[j].no);
		}
		fprintf(fout,"]\n");
	}
	fprintf(fout,"\n\n");
	if(fout!=NULL)
		fclose(fout); 
}
void addToMapRed(MapRed* map, char* word,int no,char* file )
{
	
	if(map->size==0)
	{
		map->size=1;
		map->fileMaps = (FileMap*)calloc(1,sizeof(FileMap));
		map->fileMaps->fileCountNo = 1;
		map->fileMaps->word = (char*) calloc(strlen(word)+1,sizeof(char));
		strcpy(map->fileMaps->word,word);
		
		map->fileMaps->fileCounts=(FileCount*)calloc(1,sizeof(FileCount));
		map->fileMaps->fileCounts->fileName=(char*)calloc(strlen(file)+1,sizeof(char));
		strcpy(map->fileMaps->fileCounts->fileName,file);
		
		map->fileMaps->fileCounts->no=no;
	}
	else
	{
		int i=0;
		int cmp=0;
		
		for(i=0;i<map->size && cmp<=0;i++)
		{
			cmp=strcmp(map->fileMaps[i].word,word);
			if(cmp==0)
			{
				int fileCmp=0,j=0;
				for(j=0;j<map->fileMaps[i].fileCountNo &&  fileCmp<=0;++j)
				{
					fileCmp=strcmp(map->fileMaps[i].fileCounts[j].fileName,file);
				}
				map->fileMaps[i].fileCountNo++;
				map->fileMaps[i].fileCounts = realloc(map->fileMaps[i].fileCounts,map->fileMaps[i].fileCountNo*sizeof(FileCount));
				if(!(j==map->fileMaps[i].fileCountNo-1 && fileCmp<0))
				{
					memmove(&map->fileMaps[i].fileCounts[j],&map->fileMaps[i].fileCounts[j-1],(map->fileMaps[i].fileCountNo-j)*sizeof(FileCount));
					j--;
				}
				map->fileMaps[i].fileCounts[j].fileName = (char*) calloc(strlen(file)+1,sizeof(char));
				strcpy(map->fileMaps[i].fileCounts[j].fileName,file);
				map->fileMaps[i].fileCounts[j].no=no;
				return;
			}
		}
		
		map->size++;
		map->fileMaps = realloc(map->fileMaps,map->size*sizeof(FileMap));
		if(!(i==map->size-1 && cmp <0))
		{
			memmove(&map->fileMaps[i],&map->fileMaps[i-1],(map->size-i)*sizeof(FileMap));
			i--;
		}
		map->fileMaps[i].word=(char*)calloc(strlen(word)+1,sizeof(char));
		strcpy(map->fileMaps[i].word,word);
		map->fileMaps[i].fileCountNo=1;
		map->fileMaps[i].fileCounts = (FileCount*) calloc(1,sizeof(FileCount));
		map->fileMaps[i].fileCounts[0].fileName=(char*)calloc(strlen(file)+1,sizeof(char));
		strcpy(map->fileMaps[i].fileCounts[0].fileName,file);
		map->fileMaps[i].fileCounts[0].no=no;
	}
	
}
void freeMap(MapRed* map)
{
	for(int i=0;i<map->size;++i)
	{
		for(int j=0;j<map->fileMaps[i].fileCountNo;++j)
		{
			free(map->fileMaps[i].fileCounts[j].fileName);
			map->fileMaps[i].fileCounts[j].no=0;
		}
		map->fileMaps[i].fileCountNo=0;
		
	}
	free(map->fileMaps);
	map->size=0;
	free(map);
}
void readWordsCount(char* dirName)
{
	int filesNo=0;
	
	char** files=(char**)calloc(0,sizeof(char*));
	
	DIR* dirp = opendir(dirName);
	
	struct dirent *dp;
	
	MapRed* map = (MapRed*)calloc(1,sizeof(MapRed));
	map->size=0;
	
	if(dirp == NULL)
		return;
	while((dp=readdir(dirp))!=NULL)
	{		
		if(match(dp->d_name, ".txt")!=-1)
		{
			filesNo++;
			files=realloc(files,filesNo*sizeof(char*));
			files[filesNo-1]  = (char*)calloc(strlen(dp->d_name),sizeof(char));
			strcpy(files[filesNo-1],dp->d_name);
		}
	}
	closedir(dirp);	
	for(int i=0;i<filesNo;++i)
	{
		FILE *f = NULL;
		char *line = NULL;
		unsigned int len=0;
		int read;
		char* pathToFile=(char*) calloc(strlen(dirName)+strlen(files[i])+2,sizeof(char));
		strcpy(pathToFile,dirName);
		strcat(pathToFile,"/");
		strcat(pathToFile,files[i]);
		
		f=fopen(pathToFile,"r");
		if(f==NULL)
		{
			printf(" eroare citire %s\n",pathToFile);
			return;
		}
		int isNumber=0;
		char *prevWord=NULL;
		while((read=getline(&line,&len,f))!=-1)
		{
			char *word = strtok(line," ");
			while(word!=NULL)
			{
				if(isNumber==1)
				{
					isNumber=0;
					int nr=atoi(word);
					addToMapRed(map,prevWord,nr,files[i]);
					if(prevWord!=NULL)
					{
						free(prevWord);
						prevWord=NULL;
					}
				}
				else
				{
					
					prevWord = (char*)calloc(strlen(word)+1,sizeof(char));
					
					strcpy(prevWord,word);
					isNumber=1;
				}
				
				
				word = strtok(NULL," ");
			}
			free(word);
			
		}
		if(prevWord!=NULL)
			free(prevWord);
		fclose(f);
		remove(pathToFile);
		free(pathToFile);
		free(files[i]);
		free(prevWord);
		if(line)
			free(line);
	}
	//printf("\n");
	
	char  *filePath=(char*)calloc(strlen(dirName)+1+3,sizeof(char));
	strcpy(filePath,dirName);
	strcat(filePath,".txt");
	printMapRed(map,filePath);
	
	rmdir(dirName);
	freeMap(map);
	free(filePath);
	free(files);
}

void writeWordMapToFile(WordMap *map, char *fileName, char* dirName,int rank, char** term, int subFoldersNo)
{
	int j=0;
	for(int i=0;i<subFoldersNo;++i)
	{
		char* filePath = (char*) calloc(PATH_SIZE,sizeof(char));
		strcpy(filePath,dirName);
		strcat(filePath,"/");
		strcat(filePath,term[i]);
		strcat(filePath,"/");
		strcat(filePath,fileName);
		//printf("%d : scriu in %s",rank,filePath);
		FILE* fout=NULL;
		
		int created=0;
		
		while(j<map->size  && map->termCounts[j].word[0]-term[i][2]<=0)
		{
			//printf(" %d %s %s",j,map->termCounts[j].word,term[i]);
			if(map->termCounts[j].word[0]-term[i][0]>=0 && created==0)
			{
				created=1;
				fout=fopen(filePath,"w");
				if(fout == NULL)
				{
					printf("%d eroare scriere %s\n",rank,filePath);
					return;
				}
			}
			if(fout!=NULL)
				fprintf(fout,"%s %d ",map->termCounts[j].word,map->termCounts[j].no);
			j++;
			
		}
		if(created==1 && fout!= NULL)
			fclose(fout);
		free(filePath);
	}
}

void addToWordMap(WordMap *map, char* word)
{
	if(map->size==0)
	{
		map->termCounts=(TermCount*)calloc(1,sizeof(TermCount));
		map->termCounts[0].no=1;
		map->termCounts[0].word=(char*) calloc(strlen(word)+1,sizeof(char));
		strcpy(map->termCounts[0].word,word);
		map->termCounts[0].no=1;
		map->size++;
	}
	else
	{
		int i=0;
		int cmp=0;
		for(i=0;i<map->size && cmp<=0;i++)
		{
			cmp=strcmp(map->termCounts[i].word,word);
			if(cmp==0)
			{
				map->termCounts[i].no++;
				return;
			}
		}
		map->size++;
		map->termCounts=realloc(map->termCounts,map->size*sizeof(TermCount));
		if(!(i==map->size-1 && cmp <0))
		{
			memmove(&map->termCounts[i],&map->termCounts[i-1],(map->size-i)*sizeof(TermCount));
			i--;
		}
		map->termCounts[i].word=(char*)calloc(strlen(word)+1,sizeof(char));
		strcpy(map->termCounts[i].word,word);
		map->termCounts[i].no=1;
		
	}
}

void readFileWords(char* path,int rank,char*  fileName, char* dirName,char** term,int subFoldersNo)
{
	FILE *f = NULL;
	char *line = NULL;
	unsigned int len=0;
	int read;
	f=fopen(path,"r");
	if(f==NULL)
	{
		printf("%d eroare citire %s\n",rank,path);
		return;
	}
	WordMap map;
	map.size=0;
	map.termCounts=NULL;
	
	while((read=getline(&line,&len,f))!=-1)
	{
		char* word;
		
		for(int i = 0; line[i] != '\0' && i<len; ++i)
		{
			if(line[i]>='A'&&line[i]<='Z')
			{
				line[i]='a'+(line[i]-'A');
			}
			if(!( (line[i] >= 'a' && line[i] <= 'z') || (line[i] >= 'A' && line[i] <= 'Z')|| (line[i] >= '0' && line[i] <= '9') || line[i] == '\0') )
			{
				line[i]=' ';
			}
		}	
		//printf("%d am citit linia: -%s-\n",rank,line);
		
		word = strtok(line," ");
		while(word!=NULL)
		{
			//printf("%d am citit cuvantul: -%s-\n",rank,word);
			addToWordMap(&map,word);
			word = strtok(NULL," ");
		}
		
	}
	writeWordMapToFile(&map,fileName,dirName,rank,term,subFoldersNo);
	//printWordMap(&map,rank);

	fclose(f);
	if(line)
		free(line);
}

#endif
