#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include "mpi.h"
#include "helper.h"

int main(int argc,char** argv)
{
	int count, rank, filesNo=0,workersNo;
	
	
	int subFoldersNo=7;
	char **term=(char**) calloc (subFoldersNo,sizeof(char*));
	char dirOutName0[] = "0-9";
	char dirOutName1[] = "a-d";
	char dirOutName2[] = "e-h";
	char dirOutName3[] = "i-l";
	char dirOutName4[] = "m-p";
	char dirOutName5[] = "q-t";
	char dirOutName6[] = "u-z";
	term[0]=dirOutName0;
	term[1]=dirOutName1;
	term[2]=dirOutName2;
	term[3]=dirOutName3;
	term[4]=dirOutName4;
	term[5]=dirOutName5;
	term[6]=dirOutName6;
	
	char* dirName=argv[1];
	char* dirOutName=argv[2];
	
	MPI_Request request;
	MPI_Status status;
	
	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD,&count);
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);

	workersNo=count-1;
	
	if(rank == MASTER)
	{
		
		char** files=(char**)calloc(0,sizeof(char*));
	
		char** dirOutFullName=(char**)calloc(subFoldersNo,sizeof(char*));
		mkdir(dirOutName,0700);
		for(int i=0;i<subFoldersNo;++i)
		{
			dirOutFullName[i]=(char*)calloc(strlen(dirOutName)+5,sizeof(char*));
			strcpy(dirOutFullName[i],dirOutName);
			strcat(dirOutFullName[i],"/");
			strcat(dirOutFullName[i],term[i]);
		}
		MPI_Request* req=(MPI_Request*)calloc(workersNo,sizeof(MPI_Request));
		MPI_Status*  sta=(MPI_Status*)calloc(workersNo,sizeof(MPI_Status));
		for(int i=0;i<subFoldersNo;++i)
		{
			//printf("creez %s\n",dirOutFullName[i]);
			mkdir(dirOutFullName[i],0700);
		}
		DIR* dirp = opendir(dirName);
		struct dirent *dp;
		if(dirp == NULL)
			return 0;
		while((dp=readdir(dirp))!=NULL)
		{
			
			if(match(dp->d_name, ".txt")!=-1)
			{
				filesNo++;
				files=realloc(files,filesNo*sizeof(char*));
				files[filesNo-1]  = (char*)calloc(strlen(dp->d_name),sizeof(char));
				strcpy(files[filesNo-1],dp->d_name);
			}
		}
		closedir(dirp);
		
		for(int i=0;i<filesNo;++i)
		{
			printf(" %s",files[i]);
		}
		printf("\n");
		
		int idxFile=0;
		int idxWorker=1;
		
		//printf("in total sunt %d\n",filesNo);
		while(idxFile<filesNo)
		{
			//printf("trimit la %d - %s\n",idxWorker,files[idxFile]);
			MPI_Isend(files[idxFile],strlen(files[idxFile])+1,MPI_CHAR,idxWorker,90,MPI_COMM_WORLD,&request);
			idxFile++;
			idxWorker++;
			if(idxWorker>workersNo)
			{
				idxWorker=1;
			}
		}
		
		char msg[]="end";
		for(int i=1;i<=workersNo;++i)
		{
			MPI_Isend(&msg,strlen(msg)+1,MPI_CHAR,i,90,MPI_COMM_WORLD,&request);
		}
		
		
		char* msgRec= (char*)calloc (5,sizeof(char));
		for(int i=1;i<=workersNo;++i)
		{
			MPI_Irecv(msgRec,NAME_SIZE,MPI_CHAR,i,90,MPI_COMM_WORLD,&req[i-1]);
		}
		MPI_Waitall(workersNo,req,sta);
		//procesez fisiere mapate
		//
		free(msgRec);
		int j=1;
		for(int i=0;i<subFoldersNo;++i)
		{
			
			if(j>workersNo)
			{
				j=1;
			}
			MPI_Isend(dirOutFullName[i],strlen(dirOutFullName[i])+1,MPI_CHAR,j,90,MPI_COMM_WORLD,&request);
			j++;
			
		}
		
		for(j=1;j<=workersNo;++j)
		{
			MPI_Isend(&msg,strlen(msg)+1,MPI_CHAR,j,90,MPI_COMM_WORLD,&request);
		}
		free(req);
		free(sta);
		for(int i=0;i<subFoldersNo;++i)
		{
			free(dirOutFullName[i]);
		}
		free(dirOutFullName);
		for(int i=0;i<filesNo;++i)
			free(files[i]);
		free(files);
	}
	else
	{
		char* fileToRead = (char*) calloc(NAME_SIZE,sizeof(char));
		char* filePath = (char*) calloc(PATH_SIZE,sizeof(char));
		MPI_Request req1;
		while(1)
		{
			MPI_Irecv(fileToRead,NAME_SIZE,MPI_CHAR,0,90,MPI_COMM_WORLD,&req1);
			
			strcpy(filePath,dirName);
			strcat(filePath,"/");
			MPI_Wait(&req1,&status);
			strcat(filePath,fileToRead);
			if(strcmp(fileToRead,"end")==0)
			{
				//printf("%d - %s -final\n",rank,fileToRead);
				break;
			}
			
			printf("%d - primesc fisierul %s\n",rank,filePath);
			//procesare fisier;
			
			readFileWords(filePath,rank,fileToRead,dirOutName,term,subFoldersNo);
		}
		free(fileToRead);
		free(filePath);
		char msg[]="end";
		MPI_Isend(&msg,strlen(msg)+1,MPI_CHAR,0,90,MPI_COMM_WORLD,&request);
		
		while(1)
		{
			char *dirName = (char*) calloc(PATH_SIZE,sizeof(char));
			MPI_Irecv(dirName,NAME_SIZE,MPI_CHAR,0,90,MPI_COMM_WORLD,&request);
			MPI_Wait(&request,&status);
			if(strcmp(dirName,"end")==0)
			{
				break;
			}
			readWordsCount(dirName);
			free(dirName);
		}
	}
	free(term);
	MPI_Finalize();
	return 0;	
}
